## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
###
truffle migrate --reset
truffle console
const instance = await NftMarket.deployed()
const name = await instance.name()
const symbol = await instance.symbol()
web3.eth.getStorageAt("0xXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 2)

truffle test
```

add in MetaMask network Ganache, Chain ID - 1337
