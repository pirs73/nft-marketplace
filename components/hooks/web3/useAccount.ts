import { useEffect } from 'react';
import { CryptoHookFactory } from '@app/types/hooks';
import useSwr from 'swr';

type UseAccountResponse = {
  connect: () => void;
  isLoading: boolean;
  isInstalled: boolean;
};

type AccountHookFactory = CryptoHookFactory<string, UseAccountResponse>;

export type UseAccountHook = ReturnType<AccountHookFactory>;

export const hookFactory: AccountHookFactory =
  ({ provider, ethereum, isLoading }) =>
  () => {
    const { data, mutate, isValidating, ...swr } = useSwr(
      provider ? 'web3/useAccount' : null,
      async () => {
        const accounts = await provider!.listAccounts();
        const account = accounts[0];

        if (!account) {
          throw 'Cannot retreive accaunt! Plaese, connent to web3 wallet.';
        }
        return account;
      },
      {
        revalidateOnFocus: false,
        shouldRetryOnError: false,
      },
    );

    const handleAccountsChanged = (...args: unknown[]) => {
      const accounts = args[0] as string[];

      if (accounts.length === 0) {
        console.error('Please, connect to Web3 wallet');
      } else if (accounts[0] !== data) {
        mutate(accounts[0]);
      }
    };

    useEffect(() => {
      ethereum?.on('accountsChanged', handleAccountsChanged);
      return () => {
        ethereum?.removeListener('accountsChanged', handleAccountsChanged);
      };
    });

    const connect = async () => {
      try {
        ethereum?.request({ method: 'eth_requestAccounts' });
      } catch (error) {
        console.error(error);
      }
    };

    return {
      ...swr,
      data,
      isValidating,
      isLoading: isLoading as boolean,
      isInstalled: ethereum?.isMetaMask || false,
      mutate,
      connect,
    };
  };
