import { Web3Dependencies } from '@app/types/hooks';
import { hookFactory as crateAccountHook, UseAccountHook } from './useAccount';
import { hookFactory as crateNetworkHook, UseNetworkHook } from './useNetwork';
import {
  hookFactory as crateListedNftsHook,
  UseListedNftsHook,
} from './useListedNfts';
import {
  hookFactory as createOwnedNftsHook,
  UseOwnedNftsHook,
} from './useOwnedNfts';

export type Web3Hooks = {
  useAccount: UseAccountHook;
  useNetwork: UseNetworkHook;
  useListedNfts: UseListedNftsHook;
  useOwnedNfts: UseOwnedNftsHook;
};

export type SetupHooks = {
  (d: Web3Dependencies): Web3Hooks;
};

export const setupHooks: SetupHooks = (deps) => {
  return {
    useAccount: crateAccountHook(deps),
    useNetwork: crateNetworkHook(deps),
    useListedNfts: crateListedNftsHook(deps),
    useOwnedNfts: createOwnedNftsHook(deps),
  };
};
