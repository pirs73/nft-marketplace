import useSwr from 'swr';
import { ethers } from 'ethers';
import { CryptoHookFactory } from '@app/types/hooks';
import { Nft } from '@app/types/nft';

type UseListedNftsResponse = {
  buyNft: (token: number, value: number) => Promise<void>;
};

type ListedNftsHookFactory = CryptoHookFactory<Nft[], UseListedNftsResponse>;

export type UseListedNftsHook = ReturnType<ListedNftsHookFactory>;

export const hookFactory: ListedNftsHookFactory =
  ({ contract }) =>
  () => {
    const { data, ...swr } = useSwr(
      contract ? 'web3/useListedNfts' : null,
      async () => {
        const nfts = [] as Nft[];
        const coreNfts = await contract!.getAllNftsOnSale();

        for (let i = 0; i < coreNfts.length; i++) {
          const item = coreNfts[i];
          const tokenURI = await contract!.tokenURI(item.tokenId);
          const metaRes = await fetch(tokenURI);
          const meta = await metaRes.json();

          nfts.push({
            price: parseFloat(ethers.utils.formatEther(item.price)),
            tokenId: item.tokenId.toNumber(),
            creator: item.creator,
            isListed: item.isListed,
            meta,
          });
        }

        return nfts;
      },
    );

    const buyNft = async (tokenId: number, value: number) => {
      try {
        await contract?.buyNft(tokenId, {
          value: ethers.utils.parseEther(value.toString()),
        });

        alert('You have bought Nft. See profile page.');
      } catch (error: any) {
        console.error(error.message);
      }
    };

    return {
      ...swr,
      buyNft,
      data: data || [],
    };
  };
